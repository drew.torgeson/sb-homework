# SB Homework

## Building and Running

The project is an Angular project bootstrapped using the Angular CLI. To run the project locally, run `npm run start`
and a local server will start up and host the UI at localhost:4200. To build the project for a hosted deployment, run
`npm run build:prod` and an optimized build will be generated in a dist folder.

## Project Structure

Though the requirements for this assessment were quite simple, I decided to build this application with the assumption
that it could be a foundation for a much larger application that would be maintained by many team members. As such,
a some extra engineering has been put in place to enable easy growth.

### Modules

All code not in the top level AppComponent is lazily loaded by being sectioned off into separate modules. Each module
is responsible for containing its own set of components, pages, and state files that are only used within that module.
When there are multiple modules that need to share dependencies, it would be beneficial to create a Shared module, but
for now one has not been implemented.

### Pages

Each page is an Angular component that is associated with a route in the application. Pages are composed of many other Components.

### Components

Each component is an Angular component that is not associated with a route in the application. Components will exist in
two flavors, pure and impure. A pure Component will only communicate with Inputs and Outputs and are focused on rending
HTML based on those Inputs and communicating changes through the Outputs. A pure Component will avoid dependencies on
external services to enable ease of testing. Certain exceptions will be allowed where the benefits outweigh the drawbacks,
such as with form creation. Pure Components should never interact with the NGXS store.

Impure Components are meant to wrap Pure Components and do the heavy lifting of acquiring data and handing it to the pure
Components through Inputs and responding to Output events. The impure Components will have all necessary services injected
into them and handle most of the interactions with the NGXS store.

### State

State objects represent a collection of variables that should be shared across the Module. State objects implemented in NGXS,
which is a wrapper around Redux that reduces much of the boilerplate while still allowing us to maintain a single source of
truth and uni-directional flow of data. Actions within State allow Pages and impure Components to communicate with each other
or to State objects without having to drill through many layers of Inputs and Outputs. Selectors within State allow and Pages
and impure Components to pull data from State through an Observable, guaranteeing they will always have up to date values.

### Services

Services live at the top level of the application are all injected in the root of the application. Services handle any behaviors
not specific to a component or that communicate 3rd party services or anything over HTTP.

### Styling

Styling is implemented using TailwindCSS. TailwindCSS allows you to quickly define a theme and write styles for your application
without needing to worry about the complexities of scss. It also encourages you to write many smaller, composable components,
which is something that the rest of the project structure also benefits from.

### Formatting and Linting

Formatting is done through Prettier and linting is done with tslint. Both are run on commit through git hooks to ensure all has
a similar same style before getting to the CI process.

You can manually run the code formatter with `npm run format` and the linter with `npm run lint`

### Bundle Inspection

The contents of the generated bundles can be reviewed by running `npm run bundle-report`. This is useful to see where you can
cut down on bloat within your application.

### Isolated Component Renderer

In order to make it easy to create and test and document the Components in the application, we use Storybook. Storybook is run
with `npm run storybook` and will open a browser tab showing all the components that have associated .stories.ts files. What is
displayed is a view of the component running in isolation given whatever set of data you define for it. Beyond it's generic use
cases it is also extremely useful for creating components where there is no existing data integration or for rendering hard to
trigger edge cases. It has even been provision with an addon that provides a11y tips, though I did not take time to resolve them
all for the purposes of this project.

### Potential Improvements

- Creating a base set of styled components for form fields and buttons
- Adding an interceptor to handle inject the auth token for all service calls so that it doesn't need to be hard-coded in all of them

## Testing Strategy

There are three types of tests that are represented in this code base: Snapshot, Unit, and E2E. The Snapshot and Unit tests are
run through Jest with `npm run test`. The Snapshot tests compare an existing set of DOM snapshots of the Storybook stories. It will
report to you any diffs in the DOM structure, if any, and let you update the snapshots if you're expecting a difference. The Unit
tests are your typical Jest Unit tests. The strategy for choosing what to write a Unit test for is to identify what could cause the
most harm if it isn't working properly and then cover those cases first. Then, focus on anything that could possibly need extra
documentation. This resulted in me writing tests for some of the service functions that move decimal places around as well as tests
for the front-end validation to ensure that all validators are applied correctly to the forms and that the messages they display
are the correct ones.

E2E tests are implemented in Cypress and can be run with `npm run cypress:open`. The strategy for these tests is to go through the
full flow checking simple valid and invalid use cases, without going too deep into checking all outputs. This lets us maintain a
balance of having E2E tests that are easy to write, run quickly, provide valuable feedback, don't validate what is already covered
in the Snapshot and Unit tests.

## CI/CD

Once a commit has been made to the master branch, a pipeline is triggerd in Gitlab that will run the linter, Snapshot tests, Unit
tests, and E2E tests to quickly validate everything is working as expected. To keep it simple everything is running on the same
job server, but these steps could be split out into different stages and even run in parallel.

Since this project is not set to deploy anywhere there is no CD step, but it could easily also be added as a step in the .gitlab-ci.yml
file to deploy when all CI steps are passing. This could be configured to deploy automatically or be set to wait until an authorized
user clicks a button for the deploy to continue.
