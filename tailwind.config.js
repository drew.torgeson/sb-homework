const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{ts,scss,html}'],
  darkMode: false,
  theme: {
    fontFamily: {
      display: ['Montserrat', 'Helvetica Neue', 'sans-serif'],
      body: ['Poppins', 'Helvetica Neue', 'sans-serif'],
    },
    extend: {
    },
  },
  variants: {
    extend: {
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
