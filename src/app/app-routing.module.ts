import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: 'loan',
    loadChildren: () =>
      import('./modules/loan-application/loan-application.module').then(
        (m) => m.LoanApplicationModule
      ),
  },
  {
    path: '**',
    redirectTo: '/loan/application',
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
