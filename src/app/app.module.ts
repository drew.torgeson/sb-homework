import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { NgxsRouterPluginModule } from '@ngxs/router-plugin'
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin'
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin'
import { NgxsModule } from '@ngxs/store'
import { environment } from 'src/environments/environment'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { LoanApplicationModule } from './modules/loan-application/loan-application.module'

import localeSv from '@angular/common/locales/sv'
import { registerLocaleData } from '@angular/common'
import { NgxMaskModule } from 'ngx-mask'
registerLocaleData(localeSv)

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    NgxsModule.forRoot([], {
      developmentMode: !environment.production,
      selectorOptions: {
        suppressErrors: false,
        injectContainerState: false,
      },
    }),
    NgxsSelectSnapshotModule.forRoot(),
    NgxsStoragePluginModule.forRoot({
      storage: StorageOption.SessionStorage,
      key: ['loanApplication'],
    }),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    LoanApplicationModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'sv' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
