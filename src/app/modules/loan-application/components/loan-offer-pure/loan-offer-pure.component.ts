import { Component, Input } from '@angular/core'
import { LoanOffer } from 'src/app/models/application-form.models'

@Component({
  selector: 'app-loan-offer-pure',
  templateUrl: './loan-offer-pure.component.html',
})
export class LoanOfferPureComponent {
  @Input()
  loanOffer?: LoanOffer
}
