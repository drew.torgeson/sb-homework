import { Story, Meta } from '@storybook/angular/types-6-0'

import { LoanOfferPureComponent } from './loan-offer-pure.component'

export default {
  title: 'Loan Offer Pure',
  component: LoanOfferPureComponent,
  excludeStories: /.*Data$/,
} as Meta

const Template: Story<LoanOfferPureComponent> = (args) => ({
  component: LoanOfferPureComponent,
  props: {
    ...args,
  },
})

export const Default = Template.bind({})
Default.args = {
  loanOffer: {
    loanAmount: 123000,
    interestRate: 2.5,
  },
}
