import { Story, Meta } from '@storybook/angular/types-6-0'
import { action } from '@storybook/addon-actions'

import { ApplicationFormPureComponent } from './application-form-pure.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import {
  ApplicationFormChildren,
  ApplicationFormCoapplicant,
} from 'src/app/models/application-form.models'

export default {
  title: 'Application Form Pure',
  component: ApplicationFormPureComponent,
  excludeStories: /.*Data$/,
} as Meta

export const actionsData = {
  sendRequest: action('sendRequest'),
}

const Template: Story<ApplicationFormPureComponent> = (args) => ({
  component: ApplicationFormPureComponent,
  props: {
    ...args,
    loading: false,
    sendRequest: actionsData.sendRequest,
  },
  moduleMetadata: { imports: [CommonModule, FormsModule, ReactiveFormsModule] },
})

export const Empty = Template.bind({})
Empty.args = {
  applicationForm: {},
}

export const Valid = Template.bind({})
Valid.args = {
  applicationForm: {
    monthlyIncome: 500,
    requestedAmount: 20000,
    loanTerm: 36,
    children: ApplicationFormChildren.MULTIPLE,
    coapplicant: ApplicationFormCoapplicant.MULTIPLE_BORROWERS,
  },
}
