import { ComponentFixture, TestBed } from '@angular/core/testing'
import { FormControl, ReactiveFormsModule } from '@angular/forms'

import { ApplicationFormPureComponent } from './application-form-pure.component'

describe('ApplicationFormPureComponent', () => {
  let fixture: ComponentFixture<ApplicationFormPureComponent>
  let component: ApplicationFormPureComponent
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ApplicationFormPureComponent],
    })
    fixture = TestBed.createComponent(ApplicationFormPureComponent)
    fixture.detectChanges()
    component = fixture.componentInstance
  })

  it('should create component', () => {
    expect(component).toBeDefined()
  })

  describe('Form Validators', () => {
    it('monthlyIncome should be required and greater than or equal to 500', () => {
      component.formGroup?.patchValue({
        monthlyIncome: null,
      })
      expect(component.formGroup?.get('monthlyIncome')?.hasError('required')).toBeTruthy()

      component.formGroup?.patchValue({
        monthlyIncome: 499,
      })
      expect(component.formGroup?.get('monthlyIncome')?.hasError('min')).toBeTruthy()

      component.formGroup?.patchValue({
        monthlyIncome: 500,
      })
      expect(component.formGroup?.get('monthlyIncome')?.errors).toBeFalsy()
    })

    it('requestedAmount should be required and greater than or equal to 20000', () => {
      component.formGroup?.patchValue({
        requestedAmount: null,
      })
      expect(component.formGroup?.get('requestedAmount')?.hasError('required')).toBeTruthy()

      component.formGroup?.patchValue({
        requestedAmount: 19999,
      })
      expect(component.formGroup?.get('requestedAmount')?.hasError('min')).toBeTruthy()

      component.formGroup?.patchValue({
        requestedAmount: 20000,
      })
      expect(component.formGroup?.get('requestedAmount')?.errors).toBeFalsy()
    })

    it('loanTerm should be required and between 36 and 360 (inclusive)', () => {
      component.formGroup?.patchValue({
        loanTerm: null,
      })
      expect(component.formGroup?.get('loanTerm')?.hasError('required')).toBeTruthy()

      component.formGroup?.patchValue({
        loanTerm: 35,
      })
      expect(component.formGroup?.get('loanTerm')?.hasError('min')).toBeTruthy()

      component.formGroup?.patchValue({
        loanTerm: 36,
      })
      expect(component.formGroup?.get('loanTerm')?.errors).toBeFalsy()

      component.formGroup?.patchValue({
        loanTerm: 361,
      })
      expect(component.formGroup?.get('loanTerm')?.hasError('max')).toBeTruthy()

      component.formGroup?.patchValue({
        loanTerm: 360,
      })
      expect(component.formGroup?.get('loanTerm')?.errors).toBeFalsy()
    })

    it('children should be required', () => {
      component.formGroup?.patchValue({
        children: null,
      })
      expect(component.formGroup?.get('children')?.hasError('required')).toBeTruthy()

      component.formGroup?.patchValue({
        children: 'NONE',
      })
      expect(component.formGroup?.get('children')?.errors).toBeFalsy()
    })

    it('coapplicant should be required', () => {
      component.formGroup?.patchValue({
        coapplicant: null,
      })
      expect(component.formGroup?.get('coapplicant')?.hasError('required')).toBeTruthy()

      component.formGroup?.patchValue({
        coapplicant: 'NONE',
      })
      expect(component.formGroup?.get('coapplicant')?.errors).toBeFalsy()
    })
  })

  it('getError should show an error relevant to the control', () => {
    const control = new FormControl()
    control.setErrors({
      required: true,
    })
    expect(component.getError('Test control', control)).toContain('Test control is required')

    control.setErrors({
      min: { min: 20 },
    })
    expect(component.getError('Test control', control)).toContain('Test control must be at least')

    control.setErrors({
      max: { max: 20 },
    })
    expect(component.getError('Test control', control)).toContain('Test control must be no more')

    control.setErrors({
      backend: { message: 'backend error' },
    })
    expect(component.getError('Test control', control)).toContain('backend error')
  })
})
