import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import {
  ApplicationFormError,
  ApplicationFormRequest,
} from 'src/app/models/application-form.models'

export interface ApplicationFormErrors {
  monthlyIncome: string
  requestedAmount: string
  loanTerm: string
  children: string
  coapplicant: string
}

@Component({
  selector: 'app-application-form-pure',
  templateUrl: './application-form-pure.component.html',
})
export class ApplicationFormPureComponent implements OnInit {
  @Input('applicationForm') set applicationForm(value: Partial<ApplicationFormRequest>) {
    this.error = ''
    this.formGroup = this.fb.group({
      monthlyIncome: [value.monthlyIncome, [Validators.required, Validators.min(500)]],
      requestedAmount: [value.requestedAmount, [Validators.required, Validators.min(20000)]],
      loanTerm: [value.loanTerm, [Validators.required, Validators.min(36), Validators.max(360)]],
      children: [value.children, [Validators.required]],
      coapplicant: [value.coapplicant, [Validators.required]],
    })
  }

  @Input('errors') set errors(value: ApplicationFormError | undefined) {
    if (!value) {
      this.error = ''
      if (this.formGroup) {
        Object.values(this.formGroup.controls).forEach((control) => {
          control.setErrors(null)
          control.updateValueAndValidity()
        })
      }
      return
    }

    if (value.general.code) {
      this.error = 'Could not process your request. Please try again later.'
    }

    value.fields.forEach((field) => {
      this.formGroup?.get(field.params)?.setErrors({
        backend: { message: field.message },
      })
    })
  }

  @Input()
  loading = false

  @Output()
  sendRequest = new EventEmitter<ApplicationFormRequest>()

  formGroup?: FormGroup
  error = ''

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    if (!this.formGroup) {
      this.applicationForm = {}
    }
  }

  getError(controlName: string, control: AbstractControl): string {
    if (control) {
      if (control.hasError('required')) {
        return controlName + ' is required'
      }

      if (control.hasError('min')) {
        return controlName + ' must be at least ' + control.getError('min').min
      }

      if (control.hasError('max')) {
        return controlName + ' must be no more than ' + control.getError('max').max
      }

      if (control.hasError('backend')) {
        return control.getError('backend').message
      }
    }
    return ''
  }

  onSubmit(): void {
    if (this.formGroup) {
      this.formGroup.markAllAsTouched()
      if (this.formGroup.valid) {
        this.sendRequest.emit(this.formGroup.value)
      }
    }
  }
}
