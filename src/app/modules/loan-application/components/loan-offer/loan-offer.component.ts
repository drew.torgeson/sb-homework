import { Component } from '@angular/core'
import { ViewSelectSnapshot } from '@ngxs-labs/select-snapshot'
import { LoanOffer } from 'src/app/models/application-form.models'
import { LoanApplicationState } from '../../state/loan-application.state'

@Component({
  selector: 'app-loan-offer',
  templateUrl: './loan-offer.component.html',
})
export class LoanOfferComponent {
  @ViewSelectSnapshot(LoanApplicationState.loanOffer)
  loanOffer!: LoanOffer
}
