import { Component, OnDestroy, OnInit } from '@angular/core'
import { ViewSelectSnapshot } from '@ngxs-labs/select-snapshot'
import { ActionCompletion, Actions, ofActionCompleted, Store } from '@ngxs/store'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import {
  ApplicationFormError,
  ApplicationFormRequest,
} from 'src/app/models/application-form.models'
import { LoanApplication } from '../../state/loan-application.actions'
import { LoanApplicationState } from '../../state/loan-application.state'

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
})
export class ApplicationFormComponent implements OnInit, OnDestroy {
  @ViewSelectSnapshot(LoanApplicationState.loanOffer)
  applicationForm!: ApplicationFormRequest
  errors?: ApplicationFormError

  loading = false
  destroy$ = new Subject<void>()

  constructor(private store: Store, private actions$: Actions) {}

  ngOnInit(): void {
    this.actions$
      .pipe(
        ofActionCompleted(
          LoanApplication.SubmitLoanApplicationSuccess,
          LoanApplication.SubmitLoanApplicationError
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.loading = false
      })

    this.actions$
      .pipe(ofActionCompleted(LoanApplication.SubmitLoanApplicationError), takeUntil(this.destroy$))
      .subscribe(({ action }: ActionCompletion<LoanApplication.SubmitLoanApplicationError>) => {
        this.errors = action.error
      })
  }

  onSendRequest(request: ApplicationFormRequest): void {
    this.loading = true
    this.errors = undefined
    this.store.dispatch(new LoanApplication.SubmitLoanApplication(request))
  }

  ngOnDestroy(): void {
    this.destroy$.next()
    this.destroy$.complete()
  }
}
