import { HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Action, Selector, State, StateContext } from '@ngxs/store'
import { Observable } from 'rxjs'
import { catchError, switchMap } from 'rxjs/operators'
import { ApplicationFormRequest, LoanOffer } from 'src/app/models/application-form.models'
import { LoanApplicationService } from 'src/app/services/loan-application.service'
import { LoanApplication } from './loan-application.actions'

export interface LoanApplicationStateModel {
  applicationForm?: ApplicationFormRequest
  loanOffer?: LoanOffer
}

@State({
  name: 'loanApplication',
  defaults: {
    applicationForm: undefined,
    loanOffer: undefined,
  },
})
@Injectable()
export class LoanApplicationState {
  constructor(private loanApplicationService: LoanApplicationService) {}

  @Selector()
  static applicationForm(state: LoanApplicationStateModel): ApplicationFormRequest | undefined {
    return state.applicationForm
  }

  @Selector()
  static loanOffer(state: LoanApplicationStateModel): LoanOffer | undefined {
    return state.loanOffer
  }

  @Action(LoanApplication.SubmitLoanApplication)
  submitLoanApplication(
    ctx: StateContext<LoanApplicationStateModel>,
    action: LoanApplication.SubmitLoanApplication
  ): Observable<void> {
    ctx.patchState({
      loanOffer: undefined,
    })
    return this.loanApplicationService.submitLoanApplication(action.request).pipe(
      switchMap((response: LoanOffer) =>
        ctx.dispatch(new LoanApplication.SubmitLoanApplicationSuccess(action.request, response))
      ),
      catchError((error: HttpErrorResponse) =>
        ctx.dispatch(new LoanApplication.SubmitLoanApplicationError(error.error))
      )
    )
  }

  @Action(LoanApplication.SubmitLoanApplicationSuccess)
  submitLoanApplicationSuccess(
    ctx: StateContext<LoanApplicationStateModel>,
    action: LoanApplication.SubmitLoanApplicationSuccess
  ): void {
    ctx.patchState({
      applicationForm: action.request,
      loanOffer: action.response,
    })
  }
}
