import {
  ApplicationFormError,
  ApplicationFormRequest,
  LoanOffer,
} from 'src/app/models/application-form.models'

export namespace LoanApplication {
  export class SubmitLoanApplication {
    static readonly type = '[Loan Application] Submit Loan Application'
    constructor(public request: ApplicationFormRequest) {}
  }

  export class SubmitLoanApplicationSuccess {
    static readonly type = '[Loan Application] Submit Loan Application Success'
    constructor(public request: ApplicationFormRequest, public response: LoanOffer) {}
  }

  export class SubmitLoanApplicationError {
    static readonly type = '[Loan Application] Submit Loan Application Error'
    constructor(public error: ApplicationFormError) {}
  }
}
