import { NgModule } from '@angular/core'
import { NgxsModule } from '@ngxs/store'
import { LoanApplicationState } from './state/loan-application.state'
import { LoanApplicationRoutingModule } from './loan-application-routing.module'
import { ApplicationComponent } from './pages/application/application.component'
import { ApplicationFormComponent } from './components/application-form/application-form.component'
import { ApplicationFormPureComponent } from './components/application-form-pure/application-form-pure.component'
import { LoanOfferComponent } from './components/loan-offer/loan-offer.component'
import { LoanOfferPureComponent } from './components/loan-offer-pure/loan-offer-pure.component'
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { NgxMaskModule } from 'ngx-mask'

@NgModule({
  imports: [
    NgxsModule.forFeature([LoanApplicationState]),
    NgxMaskModule.forChild(),
    LoanApplicationRoutingModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ApplicationComponent,
    ApplicationFormComponent,
    ApplicationFormPureComponent,
    LoanOfferComponent,
    LoanOfferPureComponent,
  ],
})
export class LoanApplicationModule {}
