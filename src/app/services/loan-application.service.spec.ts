import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'

import { LoanApplicationService } from './loan-application.service'

describe('LoanApplicationService', () => {
  let service: LoanApplicationService
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoanApplicationService],
    })
    service = TestBed.inject(LoanApplicationService)
  })

  describe('moveDecimal', () => {
    it('should return same number when moving 0 decimal places', () => {
      expect(service.moveDecimal(1, 0)).toBe(1)
      expect(service.moveDecimal(0, 0)).toBe(0)
      expect(service.moveDecimal(0.1, 0)).toBe(0.1)
    })

    it('should return 0 when given number is 0', () => {
      expect(service.moveDecimal(0, 1)).toBeCloseTo(0)
      expect(service.moveDecimal(0, -1)).toBeCloseTo(0)
    })

    it('should return decimal places moved to the left with negative numbers', () => {
      expect(service.moveDecimal(0.3, -1)).toBeCloseTo(3)
      expect(service.moveDecimal(3, -1)).toBeCloseTo(30)
      expect(service.moveDecimal(30, -5)).toBeCloseTo(3000000)
    })

    it('should return decimal places moved to the right with positive numbers', () => {
      expect(service.moveDecimal(0.3, 1)).toBeCloseTo(0.03)
      expect(service.moveDecimal(3, 1)).toBeCloseTo(0.3)
      expect(service.moveDecimal(30, 5)).toBeCloseTo(0.000003)
    })
  })

  describe('formatAmountForBackend', () => {
    it('should move decimal three places to left', () => {
      expect(service.formatAmountForBackend(0.1)).toBe(100)
      expect(service.formatAmountForBackend(0)).toBe(0)
      expect(service.formatAmountForBackend(1)).toBe(1000)
      expect(service.formatAmountForBackend(1.234)).toBe(1234)
    })
  })

  describe('formatAmountForUI', () => {
    it('should move decimal three places to right', () => {
      expect(service.formatAmountForUI(0.1)).toBe(0.0001)
      expect(service.formatAmountForUI(0)).toBe(0)
      expect(service.formatAmountForUI(1)).toBe(0.001)
      expect(service.formatAmountForUI(1234)).toBe(1.234)
    })
  })
})
