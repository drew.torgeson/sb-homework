import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { ApplicationFormRequest, LoanOffer } from '../models/application-form.models'

@Injectable({
  providedIn: 'root',
})
export class LoanApplicationService {
  constructor(private http: HttpClient) {}

  submitLoanApplication(request: ApplicationFormRequest): Observable<LoanOffer> {
    request.monthlyIncome = this.formatAmountForBackend(request.monthlyIncome)
    request.requestedAmount = this.formatAmountForBackend(request.requestedAmount)

    return this.http
      .post<LoanOffer>('https://homework.fdp.workers.dev/', request, {
        headers: {
          'x-api-key': 'swb-222222',
        },
      })
      .pipe(
        map((res) => ({
          interestRate: this.formatAmountForUI(res.interestRate),
          loanAmount: this.formatAmountForUI(res.loanAmount),
        }))
      )
  }

  moveDecimal(value: number, decimalPoints: number): number {
    return value / Math.pow(10, decimalPoints)
  }

  formatAmountForBackend(value: number): number {
    return this.moveDecimal(value, -3)
  }

  formatAmountForUI(value: number): number {
    return this.moveDecimal(value, 3)
  }
}
