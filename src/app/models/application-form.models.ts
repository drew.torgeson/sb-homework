export const enum ApplicationFormChildren {
  NONE = 'NONE',
  SINGLE = 'SINGLE',
  MULTIPLE = 'MULTIPLE',
}

export const enum ApplicationFormCoapplicant {
  NONE = 'NONE',
  SINGLE_BORROWER = 'SINGLE_BORROWER',
  MULTIPLE_BORROWERS = 'MULTIPLE_BORROWERS',
}

export interface ApplicationFormRequest {
  monthlyIncome: number
  requestedAmount: number
  loanTerm: number
  children: ApplicationFormChildren
  coapplicant: ApplicationFormCoapplicant
}

export interface LoanOffer {
  loanAmount: number
  interestRate: number
}

export interface ApplicationFormError {
  general: {
    code?: string
    message?: string
  }
  fields: { params: string; message: string }[]
}
