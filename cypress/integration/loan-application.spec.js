/// <reference types="cypress" />

const validLoanApplication = {
  monthlyAmount: '500',
  requestedAmount: '20000',
  loanTerm: '36',
  children: 'NONE',
  coapplicant: 'NONE'
}

describe('Loan Application Page', () => {
  beforeEach(() => {
    sessionStorage.clear()
    cy.visit('/')
  })

  it('should show loan offer when sending a successful request', () => {
    const loanOffer = {
      loanAmount: 123000000,
      interestRate: 2500,
    }
    cy.intercept('https://homework.fdp.workers.dev', loanOffer)
    cy.get('[data-cy=monthly-income]').type(validLoanApplication.monthlyAmount)
    cy.get('[data-cy=requested-amount]').type(validLoanApplication.requestedAmount)
    cy.get('[data-cy=loan-term]').type(validLoanApplication.loanTerm)
    cy.get('[data-cy=children]').select(validLoanApplication.children)
    cy.get('[data-cy=coapplicant]').select(validLoanApplication.coapplicant)
    cy.get('[data-cy=submit-btn]').click()

    cy.get('[data-cy=loan-amount]').should('be.visible')
    cy.get('[data-cy=interest-rate]').should('be.visible')
  })

  it('should show backend validation errors when receiving backend errors', () => {
    cy.intercept('https://homework.fdp.workers.dev', {
      statusCode: 400,
      body: {
        general: {},
        fields: [
          {
            params: 'monthlyIncome',
            message: 'Monthly income is too low. Minimum income is 500 EUR',
          },
          {
            params: 'requestedAmount',
            message: 'Requested Amount too low. Minimum loan of 20000 available',
          },
          {
            params: 'loanTerm',
            message: 'Loan term is too short. Minimum term: 36 months',
          },
          {
            params: 'children',
            message: 'Missing children status',
          },
          {
            params: 'coapplicant',
            message: 'Missing coapplicant status',
          },
        ],
      },
    })
    cy.get('[data-cy=monthly-income]').type(validLoanApplication.monthlyAmount)
    cy.get('[data-cy=requested-amount]').type(validLoanApplication.requestedAmount)
    cy.get('[data-cy=loan-term]').type(validLoanApplication.loanTerm)
    cy.get('[data-cy=children]').select(validLoanApplication.children)
    cy.get('[data-cy=coapplicant]').select(validLoanApplication.coapplicant)
    cy.get('[data-cy=submit-btn]').click()

    cy.get('[data-cy=monthly-income-error]').should('be.visible')
    cy.get('[data-cy=requested-amount-error]').should('be.visible')
    cy.get('[data-cy=loan-term-error]').should('be.visible')
    cy.get('[data-cy=children-error]').should('be.visible')
    cy.get('[data-cy=coapplicant-error]').should('be.visible')
  })

  it('should show general error when receiving a general error', () => {
    cy.intercept('https://homework.fdp.workers.dev', {
      statusCode: 400,
      body: {
        general: {
          code: 'TEST_CODE',
          message: 'Some unknown error',
        },
        fields: [],
      },
    })
    cy.get('[data-cy=monthly-income]').type(validLoanApplication.monthlyAmount)
    cy.get('[data-cy=requested-amount]').type(validLoanApplication.requestedAmount)
    cy.get('[data-cy=loan-term]').type(validLoanApplication.loanTerm)
    cy.get('[data-cy=children]').select(validLoanApplication.children)
    cy.get('[data-cy=coapplicant]').select(validLoanApplication.coapplicant)
    cy.get('[data-cy=submit-btn]').click()

    cy.get('[data-cy=general-error]').should('be.visible')
  })
})
